package me.gottardo.trader.model.tests;

import me.gottardo.trader.model.Position;
import me.gottardo.trader.model.Price;
import me.gottardo.trader.model.exceptions.PositionNotClosedYetException;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Andrea Gottardo on 29/03/17.
 * Copyright (c) 2017 Andrea Gottardo. All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are not permitted.
 */
class PositionTest {

    private Position testPositionOpen;
    private Position testPositionClosed;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        testPositionClosed = new Position("AAPL", new Date(2017, 02, 19), new Price(new BigDecimal("103.22")), new BigDecimal("12"), new Date(2017, 02, 23), new Price(new BigDecimal("104.64")));
        testPositionOpen = new Position("GOOG", new Date(2017, 02, 19), new Price(new BigDecimal("103.22")), new BigDecimal("12"));
        // TODO: finish this test
    }

    @org.junit.jupiter.api.Test
    void getProfit() throws PositionNotClosedYetException {
        Assertions.assertEquals(new BigDecimal("17.04"), testPositionClosed.getProfit());
    }

}