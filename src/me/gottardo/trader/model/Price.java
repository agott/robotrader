package me.gottardo.trader.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Andrea Gottardo on 29/03/17.
 * Copyright (c) 2017 Andrea Gottardo. All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are not permitted.
 */
public class Price {

    private BigDecimal price;
    private Date timestamp;

    /**
     * Constructs a price with the given price and timestamp.
     *
     * @param price
     * @param timestamp
     */
    public Price(BigDecimal price, Date timestamp) {
        this.price = price;
        this.timestamp = timestamp;
    }

    /**
     * Constructs a price with the current price. Timestamp is set to the current time.
     *
     * @param price
     */
    public Price(BigDecimal price) {
        this.price = price;
        this.timestamp = new Date(); // current time
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
