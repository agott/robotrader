package me.gottardo.trader.model.exceptions;

/**
 * Created by Andrea Gottardo on 29/03/17.
 * Copyright (c) 2017 Andrea Gottardo. All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are not permitted.
 */
public class InvalidPeriodsException extends Exception {
}
