package me.gottardo.trader.brains;

/**
 * Created by Andrea Gottardo on 29/03/17.
 * Copyright (c) 2017 Andrea Gottardo. All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are not permitted.
 */
public class PositionsBrain {

    // Singleton design pattern

    private static PositionsBrain instance = null;

    protected PositionsBrain() {
        // To defeat instantiation.
    }

    public static PositionsBrain getInstance() {
        if (instance == null) {
            instance = new PositionsBrain();
        }
        return instance;
    }

}
