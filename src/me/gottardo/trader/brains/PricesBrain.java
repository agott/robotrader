package me.gottardo.trader.brains;

import me.gottardo.trader.model.Price;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Andrea Gottardo on 29/03/17.
 * Copyright (c) 2017 Andrea Gottardo. All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are not permitted.
 */
public class PricesBrain {

    private static PricesBrain instance = null;

    // Singleton design pattern
    private HashMap<String, ArrayList<Price>> pricesMap = new HashMap<String, ArrayList<Price>>();

    protected PricesBrain() {
        // To defeat instantiation.
    }

    public static PricesBrain getInstance() {
        if (instance == null) {
            instance = new PricesBrain();
        }
        return instance;
    }

    /**
     * Adds a new price for the given stock.
     *
     * @param price the price to add
     * @param stock the stock we are adding the price for
     */
    public void addPriceForStock(Price price, String stock) {
        List<Price> stockPrices = pricesMap.get(stock);
        stockPrices.add(price);
    }

    /**
     * Returns the prices history for the given stock.
     *
     * @param stock the stock we are interested in
     * @return prices history for the given stock
     */
    public List<Price> getPricesForStock(String stock) {
        return pricesMap.get(stock);
    }
}
