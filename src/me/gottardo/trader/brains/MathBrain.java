package me.gottardo.trader.brains;

import me.gottardo.trader.model.Price;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Andrea Gottardo on 29/03/17.
 * Copyright (c) 2017 Andrea Gottardo. All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are not permitted.
 */
public class MathBrain {

    private static MathBrain instance = null;

    protected MathBrain() {
        // To defeat instantiation.
    }

    public static MathBrain getInstance() {
        if (instance == null) {
            instance = new MathBrain();
        }
        return instance;
    }

    /**
     * Returns the simple moving average for the last given periods of a prices list.
     *
     * @param prices  prices of a stock
     * @param periods
     * @return
     */
    public BigDecimal SMA(List<Price> prices, int periods) {
        List<Price> lastPrices = prices.subList(prices.size() - periods, prices.size() - 1);
        BigDecimal total = new BigDecimal(0);
        for (Price p : lastPrices) {
            total = total.add(p.getPrice());
        }
        return total.divide(new BigDecimal(periods), BigDecimal.ROUND_HALF_UP);
    }

}
